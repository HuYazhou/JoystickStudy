﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Management;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;

namespace RunJoyStickOnLocalMachine
{
    public partial class Form1 : Form
    {
        private Joystick joystick;
        private bool[] joystickButtons;
        private int n = 3, m = 4;//create n*m buttons in groupbox
        private int r = 0, c = 0;//record pressed button position
        private RadioButton[,] rb;//store n*m buttons
        private int waitTime = 10;//watit few seconds for joystcik connection
        private int countTime = 0;//count time for joystcik connection

        public Form1()
        {
            InitializeComponent();
            joystick = new Joystick(this.Handle);//create instance of joystick
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //add buttons into groupbox
            rb = new RadioButton[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    rb[i, j] = new RadioButton();
                    rb[i, j].Location = new Point(20 + j * 50, 20 + 30 * i);
                    rb[i, j].Size = new Size(50, 40);
                    rb[i, j].Text ="RB"+(m*i+1+j);            
                    rb[i, j].CheckedChanged += new System.EventHandler(this.radioBtns_Checked); //universual event process 统一的事件处理
                    groupBox1.Controls.Add(rb[i, j]); //show buttons in groupbox 在groupbox子容器中呈现控件
                }
            }

            rb[0, 0].Checked = true;// set the first radiobutton is checked defaultly
        }

        //when some radiobutton is checked, do something here
        private void radioBtns_Checked(object sender, EventArgs e)
        {
            label4.Text = "Chosed Button "+ ((RadioButton)sender).Text;
            //MessageBox.Show(((RadioButton)sender).Text + " was clicked !"); //determine the control of the event by sender;通过sender判断激发事件的控件;
        }


        //connect to joystick
        private void connectToJoystick(Joystick joystick)
        {
            while (true)
            {
                string sticks = joystick.FindJoysticks();
                if (sticks != null)
                {
                    if (joystick.AcquireJoystick(sticks))
                    {
                        enableTimer();
                        countTime = 0;
                        break;
                    }
                }
                //run out of waittime,then raise a messagebox
                if (countTime == waitTime)
                {
                    MessageBox.Show("No joystick was plugined ! ");
                    break;
                }
                //scan every second 
                countTime++;
                Console.WriteLine("countTime: "+countTime);
                Thread.Sleep(1000);
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void enableTimer()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new ThreadStart(delegate()
                {
                    timer.Enabled = true;
                }));
            }
            else
                timer.Enabled = true;
        }

        private void joystickTimer_Tick_1(object sender, EventArgs e)
        {
            try
            {
                joystick.UpdateStatus();
                joystickButtons = joystick.buttons;
                                           
                //show X value on trackbar1          
                if (joystick.Xaxis >= 0 && joystick.Xaxis <= 65535)
                {
                    label2.Text = "X: "+joystick.Xaxis;
                    trackBar1.Value = joystick.Xaxis;
                                                       
                    //When the joystick pushed to the end, then print direction in output box 
                    if (joystick.Xaxis <= 6000)
                    {
                        output.Text += "Left\n";
                        
                        //move to left radiobutton 
                        if (r >= 0 && c >= 0)
                        {
                            if (c > 0)
                                c--;
                            rb[r, c].Checked = true;
                           
                            //print row  r and column c 
                            Console.WriteLine("r: " + r + " c: " + c);
                        }
                    }


                    if (joystick.Xaxis >= 60000)
                    {
                        output.Text += "Right\n";

                        //move to right radiobutton 
                        if (r < n && c < m)
                        {
                            if (c < m - 1)
                                c++;
                            rb[r, c].Checked = true;
                            
                            Console.WriteLine("r: " + r + " c: " + c);

                        }

                    }
                        
                }
                //show Y value on trackbar2          
                if (joystick.Yaxis >= 0 && joystick.Yaxis <= 65535)
                {
                    label3.Text = "Y: " + (65535 - joystick.Yaxis);
                    trackBar2.Value = 65535-joystick.Yaxis;

                    //When the joystick pushed to the end, then print direction in output box 
                    if (joystick.Yaxis <= 6000)
                    {
                        output.Text += "Up\n";

                        //move to up radiobutton 
                        if (r >= 0 && c >= 0)
                        {
                            if (r > 0)
                                r--;
                            rb[r, c].Checked = true;
                            Console.WriteLine("r: " + r + " c: " + c);

                        }

                    }


                    if (joystick.Yaxis >= 60000)
                    {
                        output.Text += "Down\n";
                        //move to down radiobutton 
                        if (r < n && c < m)
                        {
                            if (r < n - 1)
                                r++;
                            rb[r, c].Checked = true;
                            
                            Console.WriteLine("r: " + r + " c: " + c);

                        }
                    }
                    
                }
                

                //show Z-xis rotation value on trackbar3          
                if (joystick.Zaxis >= 0 && joystick.Zaxis <= 65535)
                {
                    label5.Text = "Velocity：" + (65535 - joystick.Zaxis);
                    trackBar3.Value = 65535 - joystick.Zaxis;

                }
                //show Z value i.e. velocity on trackbar4          
                if (joystick.Zrotation >= 0 && joystick.Zrotation <= 65535)
                {
                    label6.Text = "Z: " + joystick.Zrotation;
                    trackBar4.Value = joystick.Zrotation;                 

                }

                //little joystick on the top 
                for (int k = 0; k < joystick.pointOfView.Length; k++)
                {
                    radioButton9.Checked = true;

                    if (joystick.pointOfView[k] != -1)
                    {
                        output.Text += "little button: " + joystick.pointOfView[k] + "\n";
                        int index= joystick.pointOfView[k] / 4500;
                        switch (index) {
                            case 0:
                                radioButton1.Checked = true;
                                break;
                            case 1:
                                radioButton2.Checked = true;
                                break;
                            case 2:
                                radioButton3.Checked = true;
                                break;
                            case 3:
                                radioButton4.Checked = true;
                                break;
                            case 4:
                                radioButton5.Checked = true;
                                break;
                            case 5:
                                radioButton6.Checked = true;
                                break;
                            case 6:
                                radioButton7.Checked = true;
                                break;
                            case 7:
                                radioButton8.Checked = true;
                                break;
                            
                        }

                        break;
                    }

                }
                
                //scan which function buttton is pressed
                for (int k = 0; k < joystickButtons.Length; k++)
                {
                    if (joystickButtons[k] == true)
                    {
                        output.Text += "Button " + (k+1) + " is pressed\n";
                        //update radionbutton1 state
                        //radioButton1.Checked = true;
                       // radioButton1.Text = "Function Button " + k;
                        break;
                    }
                    
                }
                //make the scrollbar that display text in output box is always at the bottom
                output.SelectionStart = output.Text.Length;
                output.Focus();
                //sleep 0.2 second
                //The machine's clock is fast, 
                //so when you hold down the function key or pushedthe joystick to the end too long, 
                //it will always output the same text
                Thread.Sleep(200);

            }
            catch
            {
                timer.Enabled = false;
                connectToJoystick(joystick);
            }
        }
        
    }
}
